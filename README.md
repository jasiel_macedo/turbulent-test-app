# turbulent-test-app

A simple application with websocket to schedule events and dispatch for connected users.

To start the server
------------

1.  Run ```npm test``` test the server communication
2.  Run ```npm start``` to start the server